import C from './constants';
import { skiDay } from './store/reducers'

const state = 0;

const action = {
  type: C.ADD_DAY,
  payload: {
    "resort": "Hevenly",
    "date": "2016/12/16",
    "powder": true,
    "backcountry": false
  }
}

const nextState = skiDay(state, action)

console.log(`
  Inital state ${state}
  action: ${JSON.stringify(action)}
  new state: ${JSON.stringify(nextState)}
`)